@extends('layouts.app')

@section('title', 'Dashboard')

@section('content')

<div id="layoutSidenav">
    <div id="layoutSidenav_content">
        <main>
            <div class="container-fluid">
                <ol class="breadcrumb mb-4 mt-4">
                    <li class="breadcrumb-item"><a href="/dashboard">Dashboard</a></li>
                    <li class="breadcrumb-item active">Profile Settings</li>
                </ol>
                <hr>
                <div class="col-12 px-3">
                    @include('partials.messages.messages')
                </div>
                @foreach($user as $data)
                <form action="{{ route('profile-settings.update', ['profile_setting' => $data->id]) }}" method="POST"
                    class="form-horizontal" role="form" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    @method('PUT')
                    <div class="card">
                        <div class="card-header">
                            <h5>Profile Info</h5>
                        </div>
                        <div class="card-body">
                            <div class="row" style="margin-top: 20px;">
                                <div class="col-md-12">
                                    <div class="image-upload">
                                        @if($data->profile_pic == '')
                                        <input type="file" name="profile_pic" class="dropify form-control"
                                            data-default-file="{{ '/storage/profile_pic/default_profile_pic.png' }}">
                                        @else
                                        <input type="file" name="profile_pic" class="dropify form-control"
                                            data-default-file="{{ '/storage/profile_pic/'.$data->profile_pic.'' }}">
                                        @endif
                                        <p class="text-center mt-10 b-700">Upload Profile Pic</p>
                                    </div>
                                </div>
                            </div>
                            <div class="row" style="margin-top: 20px;">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Name</label>
                                        <input class="form-control" type="text" name="name" placeholder="Name"
                                            value="{{ $data->name }}">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <label>Email</label>
                                    <input class="form-control" type="text" name="email" placeholder="Email"
                                        value="{{ $data->email }}">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="custom-control custom-switch">
                                            <input type="checkbox" class="custom-control-input" id="two-factor-auth" name="twoFactorAuth" {{  ($data->two_factor_option == 1 ? ' checked' : '') }}>
                                            <label class="custom-control-label" for="two-factor-auth">Enable Two Factor Authentication</label>
                                          </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12 col-md-2 col-lg-2 offset-md-4 offset-lg-4 ml-auto">
                                    <button type="submit" class="btn btn-info form-group btn-block">
                                        Update Profile
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    {{-- <div class="row">
                        <div class="col-md-3">
                            <div class="text-center">
                                <input type="file" name="profile_pic" class="dropify form-control">
                            </div>
                        </div>
                    </div> --}}
                </form>
                @endforeach
            </div>
            <hr>
        </main>
        @include('partials.footer.index')
    </div>
</div>

@endsection

@section('scripts')
<script>
    $('.dropify').dropify();
    function myFunction() {
        var x = document.getElementById("password-field");
        if (x.type === "password") {
            x.type = "text";
        } else {
            x.type = "password";
        }
    }

</script>
@endsection