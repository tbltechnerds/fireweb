@extends('layouts.app')

@section('title', 'Dashboard')

@section('content')

<div id="layoutSidenav">
    <div id="layoutSidenav_content">
        <main>
            <div class="container-fluid">
                <ol class="breadcrumb mb-4 mt-4">
                    <li class="breadcrumb-item"><a href="/dashboard">Dashboard</a></li>
                    <li class="breadcrumb-item active">Profile Settings</li>
                </ol>
                <hr>
                <div class="col-12 px-3">
                    @include('partials.messages.messages')
                </div>

                <form action="{{ route('change_password') }}" method="POST" class="form-horizontal" role="form">
                    {{ csrf_field() }}

                    <div class="card">
                        <div class="card-header">
                            <h5>Change Password</h5>
                        </div>
                        <div class="card-body">
                            <div class="row" style="margin-top: 20px;">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Current Password</label>
                                        <input class="form-control" type="password" name="current_password"
                                            placeholder="Current Password">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <label>New Password</label>
                                    <input class="form-control" type="password" name="password"
                                        placeholder="New Password">
                                </div>
                                <div class="col-md-4">
                                    <label>Confirm Password</label>
                                    <input class="form-control" type="password" name="password_confirmation"
                                        placeholder="Confirm Password">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12 col-md-2 col-lg-2 offset-md-4 offset-lg-4 ml-auto">
                                    <button type="submit" class="btn btn-info form-group btn-block">
                                        Change Password
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <hr>
        </main>
        @include('partials.footer.index')
    </div>
</div>

@endsection

@section('scripts')

@endsection