@extends('layouts.app')

@section('title', 'Dashboard')

@section('content')

<div id="layoutSidenav">
    <div id="layoutSidenav_content">
        <main>
            <div class="container-fluid">
                <ol class="breadcrumb mb-4 mt-4">
                    <li class="breadcrumb-item"><a href="/dashboard">Dashboard</a></li>
                    <li class="breadcrumb-item active">Photo List</li>
                </ol>
                <div class="row">
                    <div class="col-lg-6 col-12">
                        <h4 class="page-title mt-2 text-white">
                            Photos Table
                        </h4>
                    </div>
                    <div class="col-12 col-md-2 col-lg-2 offset-md-4 offset-lg-4">
                        <a href="{{ route('photos.create') }}" class="btn btn-info form-group btn-block">
                            Add Photo
                        </a>
                    </div>
                    <div class="col-12 px-3">
                        @include('partials.messages.messages')
                    </div>
                    <div class="col-12 mb-4">
                        <div class="card">
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table id="photos_table" class="table table-striped table-sm">
                                        <thead>
                                            <tr>
                                                <th style="width: 250px;">Photo</th>
                                                <th>Title</th>
                                                <th>Category</th>
                                                <th>Description</th>
                                                <th>Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
        @include('partials.footer.index')
    </div>
</div>

@endsection

@section('scripts')

<script>
    $(document).ready( function () {
        $.noConflict();
        $('#photos_table').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{{ route('photos.index') }}',
            "fnDrawCallback": function () {
                $('.image-popup').magnificPopup({
                type: 'image',
                closeOnContentClick: true,
                closeBtnInside: false,
                fixedContentPos: true,
                mainClass: 'mfp-with-zoom mfp-img-mobile',

                image: {
                    verticalFit: true,
                    titleSrc: function(item) {
				    return item.el.attr('title') + ' &middot; <a class="image-source-link"></a>';
			}
                },
                gallery: {
                    enabled: true
                },
                zoom: {
                    enabled: true,
                    duration: 300 
                },

            });
            },
            columns: [
                {
                    data: "photo",
                    name: "photo",
                },
                {
                    data: "title",
                    name: "title",
                },
                {
                    data: 'category_name',
                    name: 'category_name'
                },
                {
                    data: "description",
                    name: "description",
                },
                {
                    data: 'action',
                    name: 'action',
                    orderable: false
                },
                {
                    data: 'created_at',
                    name: 'created_at',
                    type: 'date',
                    targets: [4],
                    visible: false,
                    orderable: false
                }
            ],
            "order": [[ 5, 'asc']],
            "columnDefs": [
                {className: "text-center", "targets": [0]}
            ]

        });
    });
</script>

<script>
    function deleteData(id){
        
        swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover this data!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
        .then((willDelete) => {
            if (willDelete) {
                $.ajax({
                    url : "{{ url('/dashboard/photos')}}" + '/' + id,
                    type : "POST",
                    data : {'_method' : 'DELETE'},
                    headers: {
                                'X-CSRF-TOKEN' : '{{ csrf_token() }}'
                            },
                    success: function(){
                        swal({
                            title: "Success!",
                            text : "Post has been deleted \n Click OK to refresh the page",
                            icon : "success",
                        })
                        .then((s) => {
                            if (s) {
                                location.reload();
                                return false;
                            }
                        });
                    },
                    error : function(){
                        swal({
                            title: 'Opps...',
                            text : data.message,
                            type : 'error',
                            timer : '1500'
                        })
                    }
                })
            } else {
            swal("Your data is safe!");
            }
        });
    }

</script>

<script>
    function restoreData(id){
        
        swal({
            title: "Are you sure?",
            text: "You want to restore this data?",
            icon: "warning",
            buttons: true,
            dangerMode: false,
        })
        .then((willRestore) => {
            if (willRestore) {

                $.ajax({
                    url : "{{ url('/dashboard/photos/restore')}}" + '/' + id,
                    type : "POST",
                    data : {'_method' : 'POST'},
                    headers: {
                                'X-CSRF-TOKEN' : '{{ csrf_token() }}'
                            },
                    success: function(){
                        swal({
                            title: "Success!",
                            text : "Photos has been restored \n Click OK to refresh the page",
                            icon : "success",
                        })
                        .then((s) => {
                            if (s) {
                                location.reload();
                                return false;
                            }
                        });
                    },
                    error : function(){
                        swal({
                            title: 'Opps...',
                            text : data.message,
                            type : 'error',
                            timer : '1500'
                        })
                    }
                })
            } else {
            swal("Your data is safe!");
            }
        });
    }
</script>

@endsection