@extends('layouts.app')

@section('title', 'Dashboard')

@section('content')

<div id="layoutSidenav">
    <div id="layoutSidenav_content">
        <main>
            <div class="container-fluid">
                <ol class="breadcrumb mb-4 mt-4">
                    <li class="breadcrumb-item"><a href="/dashboard">Dashboard</a></li>
                    <li class="breadcrumb-item"><a href="{{ route('photos.index') }}">Photo List</a></li>
                    <li class="breadcrumb-item active">Update</li>
                </ol>

                <form action="{{ route('photos.update', $data->id) }}" method="POST" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    @method('PUT')
                    <div class="row">
                        <div class="col-lg-6 col-12">
                            <h4 class="page-title mt-2 text-white">
                                Edit Photo
                            </h4>
                        </div>
                        <div class="col-12 col-md-2 col-lg-2 offset-md-4 offset-lg-4">
                            <button type="submit" class="btn btn-info form-group btn-block">
                                Save
                            </button>
                        </div>
                        <div class="col-12 mb-4">
                            <div class="card">
                                <div class="card-body">
                                    <div class="form-group">
                                        <label for="title">Title</label>
                                        <input type="text" class="form-control" id="title" name="title"
                                            value="{{ $data->title }}">
                                    </div>
                                    <div class="form-group">
                                        <label for="description">Description</label>
                                        <textarea class="form-control" id="description" name="description"
                                            rows="3">{{ $data->description }}</textarea>
                                    </div>
                                    <div class="form-group">
                                        <label for="category">Category</label>
                                        <select class="form-control" id="category_id" name="category_id">
                                            @foreach($categories as $category)
                                            <option value="{{ $category->id }}"
                                                {{$category->category_name == $category->id  ? 'selected' : ''}}>
                                                {{ $category->category_name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="card">
                                                    <div class="card-body drop-images text-center">
                                                        <p><i class="fas fa-cloud-download-alt"></i></p>
                                                        <p class="text-black">Drag and Drop Images here or <br>Click
                                                            to
                                                            Upload</p>
                                                        <input type="file" class="drag-files" id="uploadFiles"
                                                            multiple="multiple" name="images_file[]"
                                                            accept=".gif, .png, .jpg, .jpeg">
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-12 m-t-20 my-upload-pets text-center">
                                                <label for="">Uploaded Images</label>
                                                <input type="hidden" name="album_id"
                                                    value="<?=isset($_GET['album_id'])?$_GET['album_id']:0?>">
                                                <div class="card">
                                                    <div class="card-body uploaded-images">
                                                        <div class="row uploaded-files">
                                                            <div class="col-md-12" id="emptyImg">
                                                                @if(!$data->photo)
                                                                <div class="alert alert-info" role="alert">
                                                                    Images uploaded will be display here.
                                                                </div>
                                                                @endif
                                                            </div>
                                                            @if($data->photo)
                                                            @foreach (explode('|', $data->photo) as $images)
                                                            <div class="col-md-3 img_uploaded">
                                                                <div class="website-img">
                                                                    <img src="{{ asset('/storage/uploads/photos/'.$images.'') }}"
                                                                        class="w-100" alt="Images">
                                                                </div>
                                                                <span class="cust-mod-close rmImg" title="Remove Image"
                                                                    onclick="rmimg(this)"><i
                                                                        class="fa fa-times text-white"></i></span>
                                                                <input name="old_image[]" value="{{ $images }}" hidden>
                                                            </div>
                                                            @endforeach
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </main>
        @include('partials.footer.index')
    </div>
</div>

@endsection

@section('scripts')
<script>
    var rmimg = (e) => {
        $(e).parent().remove();
        var img_uploaded = $('.img_uploaded').length;
        if(img_uploaded == 0){
        $("#emptyImg").html(''+
        '<div class="alert alert-info" role="alert">'+
            'Images uploaded will be display here.'+
            '</div>'
        );
        }
    }

    if(window.File && window.FileList && window.FileReader) {
    $("#uploadFiles").on("change", function(e) {
        var files = e.target.files,
        filesLength = files.length;
        for (var i = 0; i < filesLength; i++) {
            var f = files[i];
            var fileReader = new FileReader();
            if(f.size < 3000000){
                fileReader.onload = (function(e) {
                    $("#emptyImg").html('');
                    $(".uploaded-files").append('<div class="col-md-3 img_uploaded">'+
                        '<div class="website-img">'+
                            '<img class="w-100" src="'+e.target.result+'" alt="Pet Image">'+
                        '</div>'+
                        '<span class="cust-mod-close rmImg" title="Remove Image" onclick="rmimg(this)"><i class="fa fa-times text-white"></i></span>'+
                        '<input name="website_image[]" value="'+ e.target.result +'" hidden>'+
                    '</div>');
                });
                fileReader.readAsDataURL(f);
            } else{
                swal('Failed', "Image should less than 3 MB or 3000kb", 'warning');
            }
        }
    });
} else {
    swal('Failed', "Your browser doesn't support to File API", 'warning');
}


</script>
@endsection