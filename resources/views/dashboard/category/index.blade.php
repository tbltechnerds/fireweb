@extends('layouts.app')

@section('title', 'Dashboard')

@section('content')

<div id="layoutSidenav">
    <div id="layoutSidenav_content">
        <div class="container-fluid">
            <ol class="breadcrumb mb-4 mt-4">
                <li class="breadcrumb-item"><a href="/dashboard">Dashboard</a></li>
                <li class="breadcrumb-item active">Category</li>
            </ol>
            <div class="row">
                <div class="col-lg-6 col-12">
                    <h4 class="page-title mt-2 text-white">
                        Category List
                    </h4>
                </div>
                <div class="col-12 col-md-2 col-lg-2 offset-md-4 offset-lg-4">
                    <a href="{{ route('category.create') }}" class="btn btn-info form-group btn-block">
                        Add Category
                    </a>
                </div>
                <div class="col-12 px-3">
                    @include('partials.messages.messages')
                </div>
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="table-responsive">
                                <table id="category_table" class="table table-striped table-sm">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Category name</th>
                                            <th>Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @include('partials.footer.index')
    </div>
</div>

{{-- </main> --}}
@endsection

@section('scripts')
<script>
    $(document).ready( function () {
        $.noConflict();
        $('#category_table').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{{ route('category.index') }}',
            columns: [
                {
                    data: "id",
                    name: "id"
                },
                {
                    data: 'category_name',
                    name: 'category_name'
                },
                {
                    data: 'action',
                    name: 'action',
                    orderable: false
                },
                {
                    data: 'created_at',
                    name: 'created_at',
                    type: 'date',
                    targets: [2],
                    visible: false,
                    orderable: false
                }
            ],
            "order": [[ 3, 'asc']],
            "columnDefs": [
                {className: "text-center", "targets": [0]}
            ]

        });
    });
</script>
<script>
    function deleteData(id){
        
        swal({
            title: "Are you sure?",
            text: "You want to delete this data!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
        .then((willDelete) => {
            if (willDelete) {
                $.ajax({
                    url : "{{ url('/dashboard/category')}}" + '/' + id,
                    type : "POST",
                    data : {'_method' : 'DELETE'},
                    headers: {
                                'X-CSRF-TOKEN' : '{{ csrf_token() }}'
                            },
                    success: function(){
                        swal({
                            title: "Success!",
                            text : "Category has been deleted \n Click OK to refresh the page",
                            icon : "success",
                        })
                        .then((s) => {
                            if (s) {
                                location.reload();
                                return false;
                            }
                        });
                    },
                    error : function(){
                        swal({
                            title: 'Opps...',
                            text : data.message,
                            type : 'error',
                            timer : '1500'
                        })
                    }
                })
            } else {
            swal("Your data is safe!");
            }
        });
    }

</script>

<script>
    function restoreData(id){
        // console.log(id);
        swal({
            title: "Are you sure?",
            text: "You want to restore this data?",
            icon: "warning",
            buttons: true,
            dangerMode: false,
        })
        .then((willRestore) => {
            if (willRestore) {

                $.ajax({
                    url : "{{ url('/dashboard/category/restore')}}" + '/' + id,
                    type : "POST",
                    data : {'_method' : 'POST'},
                    headers: {
                                'X-CSRF-TOKEN' : '{{ csrf_token() }}'
                            },
                    success: function(){
                        swal({
                            title: "Success!",
                            text : "Category has been restored \n Click OK to refresh the page",
                            icon : "success",
                        })
                        .then((s) => {
                            if (s) {
                                location.reload();
                                return false;
                            }
                        });
                    },
                    error : function(){
                        swal({
                            title: 'Opps...',
                            text : data.message,
                            type : 'error',
                            timer : '1500'
                        })
                    }
                })
            } else {
            swal("Your data is safe!");
            }
        });
    }
</script>
@endsection