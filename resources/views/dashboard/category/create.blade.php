@extends('layouts.app')

@section('title', 'Dashboard')

@section('content')


<div id="layoutSidenav">
    <div id="layoutSidenav_content">
        <main>
            <div class="container-fluid">
                <ol class="breadcrumb mb-4 mt-4">
                    <li class="breadcrumb-item"><a href="/dashboard">Dashboard</a></li>
                    <li class="breadcrumb-item"><a href="{{ route('category.index') }}">Category List</a></li>
                    <li class="breadcrumb-item active">Create</li>
                </ol>

                <div class="container-fluid">
                    <form action="{{ route('category.store') }}" method="POST">
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="col-lg-6 col-12">
                                <h4 class="page-title mt-2 text-white">
                                    Add Category
                                </h4>
                            </div>
                            <div class="col-12 col-md-2 col-lg-2 offset-md-4 offset-lg-4">
                                <button type="submit" class="btn btn-info form-group btn-block">
                                    Save
                                </button>
                            </div>
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="form-group">
                                            <label for="category">Category</label>
                                            <input type="text" class="form-control" id="category" name="category"
                                                placeholder="Enter Category">
                                            @error('category') <p class="fields-error" style="color: red;">
                                                {{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </main>
        @include('partials.footer.index')
    </div>
</div>

@endsection