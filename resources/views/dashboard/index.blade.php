@extends('layouts.app')

@section('title', 'Dashboard')

@section('content')

<div id="layoutSidenav">
    <div id="layoutSidenav_content">
        <main>
            <div class="container-fluid">
                <ol class="breadcrumb mb-4 mt-4">
                    <li class="breadcrumb-item active">Dashboard</li>
                </ol>
                <div class="row">
                    <div class="col-xl-3 col-md-6">
                        <div class="card bg-primary text-white mb-4">
                            <div class="card-body">
                                @if($photos > 0)
                                <h3>{{ $photos }}</h3>
                                <p>Photos</p>
                                @else
                                <h3>0</h3>
                                <p>Photo</p>
                                @endif
                            </div>
                            <div class="icon">
                                <ion-icon name="images-outline"></ion-icon>
                            </div>
                            <div class="card-footer d-flex align-items-center justify-content-between">
                                <a class="small text-white stretched-link" href="{{ route('photos.index') }}">View
                                    Details</a>
                                <div class="small text-white">
                                    <i class="fas fa-angle-right"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-md-6">
                        <div class="card bg-warning text-white mb-4">
                            <div class="card-body">
                                <h3>0</h3>
                                <p>Product</p>
                            </div>
                            <div class="icon">
                                <ion-icon name="basket-outline"></ion-icon>
                            </div>
                            <div class="card-footer d-flex align-items-center justify-content-between">
                                <a class="small text-white stretched-link" href="#">View Details</a>
                                <div class="small text-white">
                                    <i class="fas fa-angle-right"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-md-6">
                        <div class="card bg-success text-white mb-4">
                            <div class="card-body">
                                @if($category > 0)
                                <h3>{{ $category }}</h3>
                                <p>Category</p>
                                @else
                                <h3>0</h3>
                                <p>Category</p>
                                @endif
                            </div>
                            <div class="icon">
                                <ion-icon name="apps-outline"></ion-icon>
                            </div>
                            <div class="card-footer d-flex align-items-center justify-content-between">
                                <a class="small text-white stretched-link" href="{{ route('category.index') }}">View
                                    Details</a>
                                <div class="small text-white">
                                    <i class="fas fa-angle-right"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-md-6">
                        <div class="card bg-danger text-white mb-4">
                            <div class="card-body">
                                <h3>0</h3>
                                <p>Gallery</p>
                            </div>
                            <div class="icon">
                                <ion-icon name="desktop-outline"></ion-icon>
                            </div>
                            <div class="card-footer d-flex align-items-center justify-content-between">
                                <a class="small text-white stretched-link" href="#">View Details</a>
                                <div class="small text-white">
                                    <i class="fas fa-angle-right"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>

        @include('partials.footer.index')
    </div>
</div>

@endsection