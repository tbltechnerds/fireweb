<footer class="py-4 bg-light mt-auto">
    <div class="container-fluid">
        <div class="d-flex align-items-center justify-content-between small">
            <div class="text-muted">Copyright &copy; Fire Inspection Pro 2020</div>
            <div>
                <a href="{{ route('about') }}">About</a>
                &middot;
                <a href="{{ route('index') }}">Official Homapage</a>
            </div>
        </div>
    </div>
</footer>