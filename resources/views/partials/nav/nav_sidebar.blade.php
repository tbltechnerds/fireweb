<div id="layoutSidenav">
    <div id="layoutSidenav_nav">
        <nav class="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
            <div class="sb-sidenav-menu">
                <div class="nav" style="margin-top: 21px;">
                    <a class="nav-link" href="/dashboard">
                        <div class="sb-nav-link-icon"><i class="fas fa-tachometer-alt"></i></div>
                        Dashboard
                    </a>
                    <a class="nav-link" href="{{ route('photos.index') }}">
                        <div class="sb-nav-link-icon"><i class="far fa-file-image"></i></div>
                        Photos
                    </a>
                    <a class="nav-link" href="{{ route('category.index') }}">
                        <div class="sb-nav-link-icon"><i class="fas fa-list-ul"></i></div>
                        Category
                    </a>
                    {{-- <a class="nav-link" href="{{ route('profile-settings.index') }}">
                    <div class="sb-nav-link-icon"><i class="fas fa-user-cog"></i></div>
                    Profile Settings
                    </a> --}}
                    <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseLayouts"
                        aria-expanded="false" aria-controls="collapseLayouts">
                        <div class="sb-nav-link-icon">
                            <i class="fas fa-user-cog"></i>
                        </div>
                        Profile Settings
                        <div class="sb-sidenav-collapse-arrow">
                            <i class="fas fa-angle-down"></i></div>
                    </a>
                    <div class="collapse" id="collapseLayouts" aria-labelledby="headingOne"
                        data-parent="#sidenavAccordion">
                        <nav class="sb-sidenav-menu-nested nav">
                            <a class="nav-link" href="{{ route('profile-settings.index') }}">User Profile</a>
                            <a class="nav-link" href="{{ route('change_password') }}">Change
                                Password</a>
                        </nav>
                    </div>
                </div>
            </div>
        </nav>
    </div>

</div>