<nav class="sb-topnav navbar navbar-expand navbar-dark bg-dark">
    <a class="navbar-brand" href="#"><img src="{{ asset('/storage/logo.png') }}" class="logo"
            style="position: relative; width: 50px; left: -2px; background-size: contain;"> <span
            style="font-size: 17px;">Fire Inspection Pro</span>
    </a>
    <button class="btn btn-link btn-sm order-1 order-lg-0" id="sidebarToggle" href="#">
        <i class="fas fa-bars"></i>
    </button>
    <li class="nav-item dropdown ml-auto">
        <a id="userDropdown" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            @foreach($user as $data)
            @if($data->profile_pic == '')
            <img src="{{ asset('/storage/profile_pic/default_profile_pic.png') }}" class="img">
            @else
            <img src="{{ asset('/storage/profile_pic/'.$data->profile_pic.'') }}" class="img">
            @endif
            @endforeach
        </a>
        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
            <a class="dropdown-item small" href="{{ route('profile-settings.index') }}">Profile
                Settings</a>
            <a class="dropdown-item small" href="{{ route('change_password') }}">Change
                Password</a>
            <a class="dropdown-item small" href="{{ route('logout') }}" onclick="event.preventDefault();"
                id="logout">Logout
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            </a>
        </div>
    </li>
</nav>