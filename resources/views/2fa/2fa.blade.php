@extends('layouts.auth')

@section('content')

<div class="container vh-100">
    <div class="row h-100 justify-content-center align-items-center">
        <div class="col-md-8">
            <div class="card shadow-sm">
                <div class="card-body">
                    <h5 class="card-title font-weight-bold text-center mt-3 mb-5">Enter Token</h5>

                    <div class="row">
                        <div class="col-md-6">
                            <img src="{{ asset('storage/logo.png') }}" alt="Firemarshall logo" class="img-fluid">
                        </div>

                        <div class="col-md-6 d-flex align-items-center">
                            <form action="" method="POST" class="w-100">
                                @csrf
                                
                                <div class="form-group">
                                    <label for="token">Please enter Token</label>
                                    <input type="text" name="token" placeholder="Enter received token" class="form-control {{ $errors->has('token') ? ' is-invalid' : '' }}" id="token">

                                    @error('token')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary btn-block text-uppercase">Verify</button>
                                </div>
                            </form>
                        </div>
                    </div>
                    {{-- 
                    <p class="text-center text-muted mt-5 d-flex align-items-center justify-content-center">
                        Don't have an account yet? <a class="nav-link" href="{{ route('register') }}">Sign Up</a>
                    </p> --}}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection