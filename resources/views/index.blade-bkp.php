@extends('layouts.frontend')

@section('content')

<header>
    <nav class="navbar navbar-expand-lg navbar-light">
        <div class="container">
            <a class="navbar-brand font-weight-bold text-uppercase" href="#">
                {{-- <img src="{{ asset('img/logo.png') }}" alt="Fire Inspection Pro" height="30" width="30"
                    class="img-fluid"> --}}
                Fire Marshall
            </a>

            <ul class="navbar-nav ml-auto font-weight-bold">
                <li class="nav-item active">
                    <a class="nav-link text-uppercase" href="#">Home</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link text-uppercase" href="#">Features</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link text-uppercase" href="#">FAQ</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link text-uppercase" href="#">Pricing</a>
                </li>

                <li class="nav-item ml-3">
                    <a class="btn btn-warning rounded font-weight-bold btn-rounded px-4 text-uppercase"
                        href="#">Download Now</a>
                </li>
            </ul>
        </div>
    </nav>
</header>

<section class="hero vh-100">
    <div class="container h-100">
        <div class="row justify-content-center align-items-center h-100">
            <div class="col-md-8">
                <h1 class="display-4 text-uppercase">Lorem ipsum dolor sit.</h1>
                <p class="font-italic font-weight-bold lead">Lorem ipsum dolor sit amet, consetetur.</p>
                <a class="btn btn-warning font-weight-bold btn-rounded px-4 py-2 text-uppercase" href="#">Download Now</a>
            </div>
    
            <div class="col-md-4">
                <img src="{{ asset('img/index/app-screenshot-1.png') }}" alt="" class="w-75">
            </div>
        </div>
    </div>
</section>

<section class="features py-5">
    <div class="container py-5">
        <div class="row mb-5">
            <div class="col-md-12">
                <h2 class="text-uppercase text-center display-5">WHAT CAN YOU FIND IN FIREMARSHALL</h2>
            </div>
        </div>

        <div class="row">
            <div class="col-md-3 text-center">
                <img src="{{ asset('img/index/feature-1.png') }}" class="img-fluid mb-5" alt="">
                <p class="font-weight-bold">Lorem ipsum as lorem</p>
                <p class="text-muted">Lorem ipsum as lorem</p>
            </div>

            <div class="col-md-3 text-center">
                <img src="{{ asset('img/index/feature-2.png') }}" class="img-fluid mb-5" alt="">
                <p class="font-weight-bold">Lorem ipsum as lorem</p>
                <p class="text-muted">Lorem ipsum as lorem</p>
            </div>

            <div class="col-md-3 text-center">
                <img src="{{ asset('img/index/feature-3.png') }}" class="img-fluid mb-5" alt="">
                <p class="font-weight-bold">Lorem ipsum as lorem</p>
                <p class="text-muted">Lorem ipsum as lorem</p>
            </div>

            <div class="col-md-3 text-center">
                <img src="{{ asset('img/index/feature-4.png') }}" class="img-fluid mb-5" alt="">
                <p class="font-weight-bold">Lorem ipsum as lorem</p>
                <p class="text-muted">Lorem ipsum as lorem</p>
            </div>
        </div>
    </div>
</section>

<section class="py-5">
    <div class="container py-5">
        <div class="row mb-5">
            <h2 class="display-4 font-weight-bold text-primary">App Screenshots</h2>
        </div>

        <div class="row justify-content-center align-items-center">
            <div class="col-md-3">
                <img src="{{ asset('img/index/app-screenshot-1.png') }}" alt="" class="w-75">
            </div>

            <div class="col-md-3">
                <img src="{{ asset('img/index/app-screenshot-2.png') }}" alt="" class="w-75">
            </div>

            <div class="col-md-3">
                <img src="{{ asset('img/index/app-screenshot-3.png') }}" alt="" class="w-75">
            </div>

            <div class="col-md-3">
                <img src="{{ asset('img/index/app-screenshot-4.png') }}" alt="" class="w-75">
            </div>
        </div>
    </div>
</section>

<section>
    <div class="py-5 bg-light">
        <div class="container py-5">
            <div class="row">
                <div class="col-md-6">
                    <div class="row justify-content-center">
                        <img src="{{ asset('img/index/app-screenshot-1.png') }}" alt="" class="img-fluid">
                    </div>
                </div>
                
                <div class="col-md-6">
                    <h2 class="display-4 font-weight-bold text-primary">Screen 1</h2>
                    <p class="lead">Lorem ipsum dolor sit, amet consectetur adipisicing elit. Facere nisi, quam cupiditate qui fugiat saepe nihil. Autem est adipisci tempore possimus itaque minima debitis! Esse veniam sequi culpa id cupiditate.</p>
                </div>
            </div>
        </div>
    </div>

    <div class="container py-5">
        <div class="row py-5">
            <div class="col-md-6">
                <h2 class="display-4 font-weight-bold text-primary">Screen 2</h2>
                <p class="lead">Lorem ipsum dolor sit, amet consectetur adipisicing elit. Facere nisi, quam cupiditate qui fugiat saepe nihil. Autem est adipisci tempore possimus itaque minima debitis! Esse veniam sequi culpa id cupiditate.</p>
            </div>
    
            <div class="col-md-6">
                <div class="row justify-content-center">
                    <img src="{{ asset('img/index/app-screenshot-2.png') }}" alt="" class="img-fluid">
                </div>
            </div>
        </div>
    </div>

    <div class="py-5 bg-light">
        <div class="container py-5">
            <div class="row">
                <div class="col-md-6">
                    <div class="row justify-content-center">
                        <img src="{{ asset('img/index/app-screenshot-3.png') }}" alt="" class="img-fluid">
                    </div>
                </div>
                
                <div class="col-md-6">
                    <h2 class="display-4 font-weight-bold text-primary">Screen 3</h2>
                    <p class="lead">Lorem ipsum dolor sit, amet consectetur adipisicing elit. Facere nisi, quam cupiditate qui fugiat saepe nihil. Autem est adipisci tempore possimus itaque minima debitis! Esse veniam sequi culpa id cupiditate.</p>
                </div>
            </div>
        </div>
    </div>

    <div class="container py-5">
        <div class="row py-5">
            <div class="col-md-6">
                <h2 class="display-4 font-weight-bold text-primary">Screen 4</h2>
                <p class="lead">Lorem ipsum dolor sit, amet consectetur adipisicing elit. Facere nisi, quam cupiditate qui fugiat saepe nihil. Autem est adipisci tempore possimus itaque minima debitis! Esse veniam sequi culpa id cupiditate.</p>
            </div>
    
            <div class="col-md-6">
                <div class="row justify-content-center">
                    <img src="{{ asset('img/index/app-screenshot-4.png') }}" alt="" class="img-fluid">
                </div>
            </div>
        </div>
    </div>
</section>

<footer class="footer">
    {{-- <div>
        <div class="container">
            <div class="row py-5">
                <div class="col-md-5">
                    <img src="{{ asset('img/logo.png') }}" alt="FireinspectionPro logo" height="100" width="100"
                        class="img-fluid">
                </div>

                <div class="col-md-7 d-flex flex-column">
                    <p>Subscribe and get important updates from us</p>
                    <form action="#" class="form-subscribe d-flex">
                        <input name="search" type="text" class="search-box form-control form-control-lg"
                            placeholder="Enter your email here">
                        <button name="submit" class="btn btn-warning font-weight-bold ml-2"
                            type="submit">Subscribe</button>
                    </form>
                </div>
            </div>
        </div>
    </div> --}}

    <div class="bg-footer border-bottom border-info">
        <div class="container">
            <div class="row pt-4 pb-2">
                <div class="col-md-3">
                    <ul class="list-unstyled">
                        <li>
                            <p class="font-weight-bold text-uppercase">FIREINSPECTION</p>
                        </li>
                        <li>
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Vero consequuntur facilis
                                excepturi aliquid, ipsam autem officiis dolores.</p>
                        </li>
                    </ul>
                </div>

                <div class="col-md-2">
                    <p class="lead font-weight-bold">Products</p>
                    <ul class="list-unstyled">
                        <li>Lorem ipsum/li>
                        <li>Lorem ipsum</li>
                    </ul>
                </div>

                <div class="col-md-2">
                    <p class="lead font-weight-bold">Connect</p>
                    <ul class="list-unstyled">
                        <li>Contact Us</li>
                        <li>FAQs</li>
                    </ul>
                </div>

                <div class="col-md-2">
                    <p class="lead font-weight-bold">Explore</p>
                    <ul class="list-unstyled">
                        <li>Careers</li>
                        <li>About Us</li>
                        <li>News &amp; Events</li>
                    </ul>
                </div>

                <div class="col-md-2">
                    <p class="lead font-weight-bold">Legal</p>
                    <ul class="list-unstyled">
                        <li>Privacy Policy</li>
                        <li>Terms &amp; Conditions</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="bg-footer">
        <div class="container">
            <div class="row pt-3">
                <div class="col-md-6 ml-auto">
                    <ul class="list-unstyled">
                        <li>
                            <p class="text-right">Copyright ©2020 FIREINSPECTION. All Rights Reserved</p>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</footer>

@endsection