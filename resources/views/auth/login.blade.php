@extends('layouts.app')

@section('content')

<div class="container vh-100">
    <div class="row h-100 justify-content-center align-items-center">
        <div class="col-md-8">
            <div class="card shadow-sm">
                <div class="card-body">
                    <h5 class="card-title font-weight-bold text-center mt-3 mb-5">Sign In to your account</h5>

                    <div class="row">
                        <div class="col-md-6">
                            <img src="{{ asset('storage/logo.png') }}" alt="Firemarshall logo" class="img-fluid">
                        </div>

                        <div class="col-md-6 d-flex align-items-center">
                            <form method="POST" action="{{ route('login') }}" class="w-100">
                                @csrf

                                <div class="form-group">
                                    <label for="email">Email</label>
                                    <input id="email" type="email"
                                        class="form-control @error('email') is-invalid @enderror" name="email"
                                        value="{{ old('email') }}" required autocomplete="email" autofocus>

                                    @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>

                                <div class="form-group">
                                    <label for="password">Password</label>
                                    <input id="password" type="password"
                                        class="form-control @error('password') is-invalid @enderror" name="password"
                                        required autocomplete="current-password">

                                    @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>

                                <div class="d-flex justify-content-between">
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" name="remember" id="remember"
                                            {{ old('remember') ? 'checked' : '' }}>

                                        <label class="form-check-label" for="remember">
                                            {{ __('Remember Me') }}
                                        </label>
                                    </div>

                                    <div class="form-group">
                                        @if (Route::has('password.request'))
                                        <a class="" href="{{ route('password.request') }}">
                                            {{ __('Forgot Your Password?') }}
                                        </a>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary btn-block text-uppercase">Sign
                                        in</button>
                                </div>
                            </form>
                        </div>
                    </div>
                    {{-- 
                    <p class="text-center text-muted mt-5 d-flex align-items-center justify-content-center">
                        Don't have an account yet? <a class="nav-link" href="{{ route('register') }}">Sign Up</a>
                    </p> --}}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection