@extends('layouts.frontend')
@section('title', 'Homepage')
@section('content')

@include('homepage.partials.header')
<section id="intro">
    <div class="intro-container">
        <div id="introCarousel" class="carousel  slide carousel-fade" data-ride="carousel">
            <ol class="carousel-indicators"></ol>
            <div class="carousel-inner" role="listbox">
                <div class="carousel-item active"
                    style="background-image: url('{{ asset('frontpage/img/intro-carousel/main1.jpg') }}');">
                    <div class="carousel-container">
                        <div class="carousel-content">
                            <div class="intro-logo">
                                <img src="{{ asset('frontpage/img/comp-logo.png') }}" alt="Company Logo">
                            </div>
                            <h1>WELCOME TO FIRE INSPECTION PRO</h1>
                            <p>Automatic fire-extinguishing systems, other than automatic sprinkler systems, shall be
                                designed, installed, inspected, tested and maintained in accordance with the provisions
                                of this section and the applicable referenced standards.</p>
                            <a href="#app-features" class="btn-get-started scrollto">Get Started</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section><!-- #intro -->

<main id="main">

    <section id="gallery" class="wow fadeInUp app-features">
        <div class="container">
            <header class="section-header">
                <h3>GALLERY</h3>
            </header>
            @if (count($photos)>0)
            <div class="owl-carousel gallery-slider">
                @foreach ($photos as $photo)
                @php
                $images = explode('|', $photo->photo);
                $imgUrl = $photo->photo ? 'photos/'.$images[0] : 'default_image.png';
                @endphp
                <a onclick="viewImage({{ $photo->id }})" href="javascript:;" title="{{ $photo->title }}">
                    <div class="home-gallery"><img src="{{ asset('/storage/uploads/' . $imgUrl) }}"
                            alt="{{ $photo->title }}"></div>
                </a>
                @endforeach
            </div>
            <div class="text-center mt-20"><a href="{{ route('firesafety') }}" class="btn-get-started scrollto">Browse
                    Gallery</a></div>
            @else
            <div class="col-md-12">
                <div class="alert alert-info"><b>Empty!</b> There are no images to display. </div>
            </div>
            @endif
        </div>
    </section><!-- #gallery -->

    <section id="about">
        <div class="container">
            <header class="section-header">
                <h3>The International Fire Code Commercial Kitchen Hood Systems</h3>

                <h4 class="title text-left text-black">901.7 Systems Out of Service</h4>
                <p class="text-left">
                    Where a required <a
                        href="https://up.codes/viewer/virginia/va-fire-code-2012/chapter/2/definitions#fire_protection_system"
                        target="_blank">fire protection system</a> is out of service, the fire department and the <a
                        href="https://up.codes/viewer/virginia/va-fire-code-2012/chapter/2/definitions#fire_code_official"
                        target="_blank">fire code official</a> shall be notified immediately and, where required by the
                    <a href="https://up.codes/viewer/virginia/va-fire-code-2012/chapter/2/definitions#fire_code_official"
                        target="_blank">fire code official</a>, the building shall either be evacuated or an approved <a
                        href="https://up.codes/viewer/virginia/va-fire-code-2012/chapter/2/definitions#fire_watch">fire
                        watch</a> shall be provided for all occupants left unprotected by the shutdown until the <a
                        href="https://up.codes/viewer/virginia/va-fire-code-2012/chapter/2/definitions#fire_protection_system"
                        target="_blank">fire protection system</a> has been returned to service.
                </p>

                <h4 class="title text-left text-black">904.1 General</h4>
                <p class="text-left">
                    <a href="https://up.codes/viewer/virginia/va-fire-code-2012/chapter/2/definitions#automatic_fire_extinguishing_system"
                        target="_blank">Automatic fire-extinguishing systems</a>, other than <a
                        href="https://up.codes/viewer/virginia/va-fire-code-2012/chapter/2/definitions#automatic_sprinkler_system"
                        target="_blank">automatic sprinkler systems</a>, shall be designed, installed, inspected, tested
                    and maintained in accordance with the provisions of this section and the applicable referenced
                    standards.
                </p>

                <h4 class="title text-left text-black">904.1.1 Certification of Service Personnel for Fire-Extinguishing
                    Equipment</h4>
                <p class="text-left">
                    Service personnel providing or conducting maintenance on <a
                        href="https://up.codes/viewer/virginia/va-fire-code-2012/chapter/2/definitions#automatic_fire_extinguishing_system"
                        target="_blank">automatic fire-extinguishing systems</a>, other than <a
                        href="https://up.codes/viewer/virginia/va-fire-code-2012/chapter/2/definitions#automatic_sprinkler_system"
                        target="_blank">automatic sprinkler systems</a>, shall possess a valid certificate issued by an
                    approved governmental <a
                        href="https://up.codes/viewer/virginia/va-fire-code-2012/chapter/2/definitions#agency"
                        target="_blank">agency</a>, or other approved organization for the type of <a
                        href="https://up.codes/viewer/virginia/va-fire-code-2012/chapter/2/definitions#system"
                        target="_blank">system</a> and work performed.
                </p>
                <div class="text-center"><a href="https://up.codes/viewer/virginia/ifc-2015" target="_blank"
                        class="btn-get-started scrollto">Read more about The International Fire Code Commercial Kitchen
                        Hood Systems</a></div>
            </header>
        </div>
    </section><!-- #about -->

    <section id="app-features" class="wow fadeInUp app-features">
        <div class="container">
            <header class="section-header">
                <h3>APP FEATURES</h3>
            </header>
            <div class="owl-carousel app-slider">
                <a href="{{ asset('frontpage/img/screenshots/f1.png') }}" data-lightbox="portfolio" data-title="App 1"
                    class="link-preview" title="Preview"><img src="{{ asset('frontpage/img/screenshots/f1.png') }}"
                        itemprop="thumbnail" alt="App Features"></a>
                <a href="{{ asset('frontpage/img/screenshots/f2.png') }}" data-lightbox="portfolio" data-title="App 2"
                    class="link-preview" title="Preview"><img src="{{ asset('frontpage/img/screenshots/f2.png') }}"
                        itemprop="thumbnail" alt="App Features"></a>
                <a href="{{ asset('frontpage/img/screenshots/f3.png') }}" data-lightbox="portfolio" data-title="App 3"
                    class="link-preview" title="Preview"><img src="{{ asset('frontpage/img/screenshots/f3.png') }}"
                        itemprop="thumbnail" alt="App Features"></a>
                <a href="{{ asset('frontpage/img/screenshots/f4.png') }}" data-lightbox="portfolio" data-title="App 4"
                    class="link-preview" title="Preview"><img src="{{ asset('frontpage/img/screenshots/f4.png') }}"
                        itemprop="thumbnail" alt="App Features"></a>
            </div>
        </div>
    </section><!-- #app-features -->

    <!-- Preview Images Modal -->
    @include('homepage.preview-image')

    <!-- Footer -->
    @include('homepage.partials.download')
</main>
@endsection

@push('scripts')
<script src="{{ asset('frontpage/js/init_homepage.js') }}"></script>
@endpush