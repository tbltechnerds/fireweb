<header id="header" class="{{ request()->is('/') ? '' : 'fix-scrolled' }}">
    <div class="container-fluid">
        <div id="logo" class="pull-left">
            <div class="comp-logo">
                <a href="{{ route('index') }}"><img src="{{ asset('frontpage/img/comp-logo.png') }}" alt="Company Logo" title="Company Logo" /></a>
            </div>
            <a href="{{ route('index') }}" class="comp-name">Fire Inspection Pro</a>
        </div>
        <nav id="nav-menu-container">
            <ul class="nav-menu">
                <li class="{{ request()->is('/') ? 'menu-active' : '' }}"><a href="{{ route('index') }}#intro">Home</a></li>
                <li class="{{ request()->is('firesafety') ? 'menu-active' : '' }}"><a href="{{ route('firesafety') }}">Gallery</a></li>
                <li class="{{ request()->is('about') ? 'menu-active' : '' }}"><a href="{{ route('about') }}">About</a></li>
                <li class="{{ request()->is('app-features') ? 'menu-active' : '' }}"><a href="{{ route('index') }}#app-features">App Features</a></li>
                <li class="{{ request()->is('download') ? 'menu-active' : '' }}"><a href="{{ route('index') }}#download-section">Download</a></li>
            </ul>
        </nav><!-- #nav-menu-container -->
    </div>
</header><!-- #header -->
