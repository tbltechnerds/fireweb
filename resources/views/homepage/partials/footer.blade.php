<footer id="footer">
    <div class="container">
        <div class="copyright">
            &copy; Copyright <strong>Fire Inspection Pro</strong>. All Rights Reserved
        </div>
        <div class="credits">
            Developed by <a href="http://tbltechnerds.com/">Denver Web Development</a>
        </div>
    </div>
</footer><!-- #footer -->

<a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>


<div class="cookieConsent" id="cookieConsent">
    <div class="closeCookieConsent" id="closeCookieConsent"><i class="fa fa-times"></i></div>
    <div class="row">
        <div class="col-md-9">
            We are using cookies on our website. View our Cookie Statement. <a href="javascript:;" target="_blank">More info</a>.
        </div>
        <div class="col-md-3 text-center">
            <a class="cookieConsentOK">Accept Cookies</a>
        </div>
    </div>
</div>
