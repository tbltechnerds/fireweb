<section id="download-section" class="download-sections">
    <div class="container text-center">
        <h3>App Coming Soon</h3>
        <a href="javascript:;" target="_blank">
            <img src="{{ asset('frontpage/img/app_store.png') }}" alt="App Store Button">
        </a>
        <a href="javascript:;" target="_blank">
            <img src="{{ asset('frontpage/img/googleplay.png') }}" alt="Google Play Store Button">
        </a>
    </div>
</section>
