@extends('layouts.frontend')
@section('title', 'Gallery')
@section('content')

<main id="main" class="mt-20">

    <section id="app-features" class="wow fadeInUp">
        <div class="container-fluid">
            <header class="section-header">
                <h3>GALLERY</h3>
            </header>

            <div class="my-gallery row">
                @if (count($photos)>0)
                    @foreach ($photos as $photo)
                        @php
                            $images = explode('|', $photo->photo);
                            $imgUrl = $photo->photo ? 'photos/'.$images[0] : 'default_image.png';
                        @endphp
                        <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                            <a onclick="viewImage({{ $photo->id }})" href="javascript:;">
                                <div class="gal-img">
                                    <img src="{{ asset('/storage/uploads/'.$imgUrl) }}" itemprop="thumbnail" alt="Image description" />
                                    <span class="gal-title">{{ $photo->title }}</span>
                                </div>
                            </a>
                        </div>
                    @endforeach
                @else
                    <div class="col-md-12">
                        <div class="alert alert-info"><b>Empty!</b> There are no images to display. </div>
                    </div>
                @endif
            </div>

            <!-- Default Swipe Display -->
            <div class="pswp" tabindex="-1" role="dialog" aria-hidden="true">
                <!-- Background of PhotoSwipe.
                    It's a separate element, as animating opacity is faster than rgba(). -->
                <div class="pswp__bg"></div>

                <!-- Slides wrapper with overflow:hidden. -->
                <div class="pswp__scroll-wrap">

                    <!-- Container that holds slides. PhotoSwipe keeps only 3 slides in DOM to save memory. -->
                    <!-- don't modify these 3 pswp__item elements, data is added later on. -->
                    <div class="pswp__container">
                        <div class="pswp__item"></div>
                        <div class="pswp__item"></div>
                        <div class="pswp__item"></div>
                    </div>

                    <!-- Default (PhotoSwipeUI_Default) interface on top of sliding area. Can be changed. -->
                    <div class="pswp__ui pswp__ui--hidden">

                        <div class="pswp__top-bar">

                            <!--  Controls are self-explanatory. Order can be changed. -->

                            <div class="pswp__counter"></div>

                            <button class="pswp__button pswp__button--close" title="Close (Esc)"></button>

                            <button class="pswp__button pswp__button--share" title="Share"></button>

                            <button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button>

                            <button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button>

                            <!-- Preloader demo https://codepen.io/dimsemenov/pen/yyBWoR -->
                            <!-- element will get class pswp__preloader--active when preloader is running -->
                            <div class="pswp__preloader">
                                <div class="pswp__preloader__icn">
                                <div class="pswp__preloader__cut">
                                    <div class="pswp__preloader__donut"></div>
                                </div>
                                </div>
                            </div>
                        </div>

                        <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
                            <div class="pswp__share-tooltip"></div>
                        </div>

                        <button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)">
                        </button>

                        <button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)">
                        </button>

                        <div class="pswp__caption">
                            <div class="pswp__caption__center"></div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Default Swipe Display -->
        </div>
    </section><!-- #Screenhsots -->

    <!-- Preview Images Modal -->
    @include('homepage.preview-image')

    <!-- Footer -->
</main>

@endsection

@push('scripts')
    <script src="{{ asset('frontpage/js/init_homepage.js') }}"></script>
@endpush
