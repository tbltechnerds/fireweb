<!-- View Image Post  with comments and likes -->
<div class="modal fade font-baloo" id="viewImageModal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="view-img-close-btn" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <div class="view-container-left">
                    <div class="owl-carousel single-slider" id="item-images"></div>
                </div>
                <div class="view-container-right">
                    <div class="new-user-list no-shadow b-0">
                        <div class="post-wrapper-title" id="item-title"></div>
                    </div>
                    <div class="image-post-description" id="item-desc"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Close Preview Images Modal -->
