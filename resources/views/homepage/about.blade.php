@extends('layouts.frontend')
@section('title', 'About')
@section('content')

@include('homepage.partials.header')

<main id="main" class="mt-100">

    <section id="about" style="background: none;">
        <div class="container">
            <header class="section-header">
                <h3>The International Fire Code Commercial Kitchen Hood Systems</h3>

                <h4 class="title text-left text-black">901.7 Systems Out of Service</h4>
                <p class="text-left">
                    Where a required <a href="https://up.codes/viewer/virginia/va-fire-code-2012/chapter/2/definitions#fire_protection_system" target="_blank">fire protection system</a> is out of service, the fire department and the <a href="https://up.codes/viewer/virginia/va-fire-code-2012/chapter/2/definitions#fire_code_official" target="_blank">fire code official</a> shall be notified immediately and, where required by the <a href="https://up.codes/viewer/virginia/va-fire-code-2012/chapter/2/definitions#fire_code_official" target="_blank">fire code official</a>, the building shall either be evacuated or an approved <a href="https://up.codes/viewer/virginia/va-fire-code-2012/chapter/2/definitions#fire_watch">fire watch</a> shall be provided for all occupants left unprotected by the shutdown until the <a href="https://up.codes/viewer/virginia/va-fire-code-2012/chapter/2/definitions#fire_protection_system" target="_blank">fire protection system</a> has been returned to service.
                </p>

                <h4 class="title text-left text-black">904.1 General</h4>
                <p class="text-left">
                    <a href="https://up.codes/viewer/virginia/va-fire-code-2012/chapter/2/definitions#automatic_fire_extinguishing_system" target="_blank">Automatic fire-extinguishing systems</a>, other than <a href="https://up.codes/viewer/virginia/va-fire-code-2012/chapter/2/definitions#automatic_sprinkler_system" target="_blank">automatic sprinkler systems</a>, shall be designed, installed, inspected, tested and maintained in accordance with the provisions of this section and the applicable referenced standards.
                </p>

                <h4 class="title text-left text-black">904.1.1 Certification of Service Personnel for Fire-Extinguishing Equipment</h4>
                <p class="text-left">
                    Service personnel providing or conducting maintenance on <a href="https://up.codes/viewer/virginia/va-fire-code-2012/chapter/2/definitions#automatic_fire_extinguishing_system" target="_blank">automatic fire-extinguishing systems</a>, other than <a href="https://up.codes/viewer/virginia/va-fire-code-2012/chapter/2/definitions#automatic_sprinkler_system" target="_blank">automatic sprinkler systems</a>, shall possess a valid certificate issued by an approved governmental <a href="https://up.codes/viewer/virginia/va-fire-code-2012/chapter/2/definitions#agency" target="_blank">agency</a>, or other approved organization for the type of <a href="https://up.codes/viewer/virginia/va-fire-code-2012/chapter/2/definitions#system" target="_blank">system</a> and work performed.
                </p>
                <h4 class="title text-left text-black">904.2 Where Required</h4>
                <p class="text-left">
                    <a href="https://up.codes/viewer/virginia/va-fire-code-2012/chapter/2/definitions#automatic_fire_extinguishing_system" target="_blank" rel="noopener noreferrer">Automatic fire-extinguishing systems</a> installed as an alternative to the required <a href="https://up.codes/viewer/virginia/va-fire-code-2012/chapter/2/definitions#automatic_sprinkler_system" target="_blank" rel="noopener noreferrer">automatic sprinkler systems</a> of <a href="https://up.codes/viewer/virginia/va-fire-code-2012/chapter/9/fire-protection-systems#903" target="_blank" rel="noopener noreferrer">Section 903</a> shall be approved by the <a href="https://up.codes/viewer/virginia/va-fire-code-2012/chapter/2/definitions#fire_code_official" target="_blank" rel="noopener noreferrer">fire code official</a>. <a href="https://up.codes/viewer/virginia/va-fire-code-2012/chapter/2/definitions#automatic_fire_extinguishing_system" target="_blank" rel="noopener noreferrer">Automatic fire-extinguishing systems</a> shall not be considered alternatives for the purposes of exceptions or reductions allowed by other requirements of this code.
                </p>
                <h4 class="title text-left text-black">904.2.1 Commercial Hood and Duct Systems</h4>
                <p class="text-left">
                    Each required commercial kitchen exhaust <a href="https://up.codes/viewer/virginia/va-fire-code-2012/chapter/2/definitions#hood" target="_blank" rel="noopener noreferrer">hood</a> and duct <a href="https://up.codes/viewer/virginia/va-fire-code-2012/chapter/2/definitions#system" target="_blank" rel="noopener noreferrer">system</a> required by <a href="https://up.codes/viewer/virginia/va-fire-code-2012/chapter/6/building-services-and-systems#609" target="_blank" rel="noopener noreferrer">Section 609</a> to have a <a href="https://up.codes/viewer/virginia/va-fire-code-2012/chapter/2/definitions#type_i" target="_blank" rel="noopener noreferrer">Type I hood</a> shall be protected with an approved <a href="https://up.codes/viewer/virginia/va-fire-code-2012/chapter/2/definitions#automatic_fire_extinguishing_system" target="_blank" rel="noopener noreferrer">automatic fire-extinguishing system</a> installed in accordance with this code.
                </p>
                <h4 class="title text-left text-black">904.3 Installation</h4>
                <p class="text-left">
                    <a href="https://up.codes/viewer/virginia/va-fire-code-2012/chapter/2/definitions#automatic_fire_extinguishing_system" target="_blank" rel="noopener noreferrer">Automatic fire-extinguishing systems</a> shall be installed in accordance with this section.
                </p>
                <h4 class="title text-left text-black">904.3.1 Electrical Wiring</h4>
                <p class="text-left">
                    Electrical wiring shall be in accordance with NFPA 70.
                </p>
                <h4 class="title text-left text-black">904.3.2 Actuation</h4>
                <p class="text-left">
                    <a href="https://up.codes/viewer/virginia/va-fire-code-2012/chapter/2/definitions#automatic_fire_extinguishing_system" target="_blank" rel="noopener noreferrer">Automatic fire-extinguishing systems</a> shall be automatically actuated and provided with a manual means of actuation in accordance with <a href="https://up.codes/viewer/virginia/va-fire-code-2012/chapter/9/fire-protection-systems#904.11.1" target="_blank" rel="noopener noreferrer">Section 904.11</a>.1. Where more than one hazard could be simultaneously involved in fire due to their proximity, all hazards shall be protected by a single <a href="https://up.codes/viewer/virginia/va-fire-code-2012/chapter/2/definitions#system" target="_blank" rel="noopener noreferrer">system</a> designed to protect all hazards that could become involved. <br><br>
                    <b>Exception:</b> Multiple <a href="https://up.codes/viewer/virginia/va-fire-code-2012/chapter/2/definitions#system" target="_blank" rel="noopener noreferrer">systems</a> shall be permitted to be installed if they are designed to operate simultaneously.
                </p>
                <h4 class="title text-left text-black">904.3.3 System Interlocking</h4>
                <p class="text-left">
                    <a href="https://up.codes/viewer/virginia/va-fire-code-2012/chapter/2/definitions#automatic" target="_blank" rel="noopener noreferrer">Automatic</a> equipment interlocks with fuel shutoffs, <a href="https://up.codes/viewer/virginia/va-fire-code-2012/chapter/2/definitions#ventilation" target="_blank" rel="noopener noreferrer">ventilation</a> controls, door closers, window shutters, conveyor openings, smoke and heat vents, and other features necessary for proper operation of the fire-extinguishing <a href="https://up.codes/viewer/virginia/va-fire-code-2012/chapter/2/definitions#system" target="_blank" rel="noopener noreferrer">system</a> shall be provided as required by the <a href="https://up.codes/viewer/virginia/va-fire-code-2012/chapter/2/definitions#design" target="_blank" rel="noopener noreferrer">design</a> and installation standard utilized for the hazard.
                </p>
                <h4 class="title text-left text-black">904.3.4 Alarms and Warning Signs</h4>
                <p class="text-left">
                    Where alarms are required to indicate the operation of <a href="https://up.codes/viewer/virginia/va-fire-code-2012/chapter/2/definitions#automatic_fire_extinguishing_system" target="_blank" rel="noopener noreferrer">automatic fire-extinguishing systems</a>, distinctive audible, visible alarms and warning signs shall be provided to warn of pending <a href="https://up.codes/viewer/virginia/va-fire-code-2012/chapter/2/definitions#agent" target="_blank" rel="noopener noreferrer">agent</a> discharge. Where exposure to <a href="https://up.codes/viewer/virginia/va-fire-code-2012/chapter/2/definitions#automatic" target="_blank" rel="noopener noreferrer">automatic</a>-extinguishing <a href="https://up.codes/viewer/virginia/va-fire-code-2012/chapter/2/definitions#agent" target="_blank" rel="noopener noreferrer">agents</a> poses a hazard to persons and a delay is required to ensure the evacuation of occupants before agent discharge, a separate warning signal shall be provided to alert occupants once <a href="https://up.codes/viewer/virginia/va-fire-code-2012/chapter/2/definitions#agent" target="_blank" rel="noopener noreferrer">agent</a> discharge has begun. Audible signals shall be in accordance with <a href="https://up.codes/viewer/virginia/va-fire-code-2012/chapter/9/fire-protection-systems#907.5.2" target="_blank" rel="noopener noreferrer">Section 907.5.2</a>.
                </p>
                <h4 class="title text-left text-black">904.5 Wet-Chemical Systems</h4>
                <p class="text-left">
                    Wet-<a href="https://up.codes/viewer/virginia/va-fire-code-2012/chapter/2/definitions#chemical" target="_blank" rel="noopener noreferrer">chemical</a> extinguishing <a href="https://up.codes/viewer/virginia/va-fire-code-2012/chapter/2/definitions#system" target="_blank" rel="noopener noreferrer">systems</a> shall be installed, maintained, periodically inspected and tested in accordance with NFPA 17A and their listing.
                </p>
                <h4 class="title text-left text-black">904.5.1 System Test</h4>
                <p class="text-left">
                    <a href="https://up.codes/viewer/virginia/va-fire-code-2012/chapter/2/definitions#system" target="_blank" rel="noopener noreferrer">Systems</a> shall be inspected and tested for proper operation at six-month intervals. Tests shall include a check of the detection <a href="https://up.codes/viewer/virginia/va-fire-code-2012/chapter/2/definitions#system" target="_blank" rel="noopener noreferrer"></a>, alarms and releasing devices, including manual stations and other associated equipment. Extinguishing <a href="https://up.codes/viewer/virginia/va-fire-code-2012/chapter/2/definitions#system" target="_blank" rel="noopener noreferrer">system</a> units shall be weighed and the required amount of <a href="https://up.codes/viewer/virginia/va-fire-code-2012/chapter/2/definitions#agent" target="_blank" rel="noopener noreferrer">agent</a> verified. Stored pressure-type units shall be checked for the required pressure. The cartridge of cartridge-operated units shall be weighed and replaced at intervals indicated by the manufacturer.
                </p>
                <h4 class="title text-left text-black">904.5.2 Fusible Link Maintenance</h4>
                <p class="text-left">
                    Fixed temperature-sensing elements shall be maintained to ensure proper operation of the <a href="https://up.codes/viewer/virginia/va-fire-code-2012/chapter/2/definitions#system" target="_blank" rel="noopener noreferrer">system</a>.
                </p>
                <h4 class="title text-left text-black">904.11 Commercial Cooking Systems</h4>
                <p class="text-left">
                    The <a href="https://up.codes/viewer/virginia/va-fire-code-2012/chapter/2/definitions#automatic_fire_extinguishing_system" target="_blank" rel="noopener noreferrer">automatic fire-extinguishing system</a> for commercial cooking <a href="https://up.codes/viewer/virginia/va-fire-code-2012/chapter/2/definitions#system" target="_blank" rel="noopener noreferrer">systems</a> shall be of a type recognized for protection of commercial cooking equipment and exhaust <a href="https://up.codes/viewer/virginia/va-fire-code-2012/chapter/2/definitions#system" target="_blank" rel="noopener noreferrer">systems</a> of the type and arrangement protected. Pre-engineered <a href="https://up.codes/viewer/virginia/va-fire-code-2012/chapter/2/definitions#automatic_dry" target="_blank" rel="noopener noreferrer">automatic dry</a> - and wet-<a href="https://up.codes/viewer/virginia/va-fire-code-2012/chapter/2/definitions#chemical" target="_blank" rel="noopener noreferrer">chemical</a> extinguishing <a href="https://up.codes/viewer/virginia/va-fire-code-2012/chapter/2/definitions#system" target="_blank" rel="noopener noreferrer">systems</a> shall be tested in accordance with UL 300 and listed and labeled for the intended application. Other types of <a href="https://up.codes/viewer/virginia/va-fire-code-2012/chapter/2/definitions#automatic_fire_extinguishing_system" target="_blank" rel="noopener noreferrer">automatic fire-extinguishing systems</a> shall be listed and labeled for specific use as protection for commercial cooking operations. The <a href="https://up.codes/viewer/virginia/va-fire-code-2012/chapter/2/definitions#system" target="_blank" rel="noopener noreferrer">system</a> shall be installed in accordance with this code, its listing and the manufacturer's installation instructions. <a href="https://up.codes/viewer/virginia/va-fire-code-2012/chapter/2/definitions#automatic_fire_extinguishing_system" target="_blank" rel="noopener noreferrer">Automatic fire-extinguishing systems</a> of the following types shall be installed in accordance with the referenced standard indicated, as follows: <br><br>

                    1.	<a href="https://up.codes/viewer/virginia/va-fire-code-2012/chapter/2/definitions#carbon_dioxide_extinguishing_system" target="_blank" rel="noopener noreferrer">Carbon dioxide extinguishing systems</a>, NFPA 12.<br>
                    2.	<a href="https://up.codes/viewer/virginia/va-fire-code-2012/chapter/2/definitions#automatic_sprinkler_system" target="_blank" rel="noopener noreferrer"></a>, NFPA 13.<br>
                    3.	Foam-water sprinkler <a href="https://up.codes/viewer/virginia/va-fire-code-2012/chapter/2/definitions#system" target="_blank" rel="noopener noreferrer">system</a> or foam-water spray <a href="https://up.codes/viewer/virginia/va-fire-code-2012/chapter/2/definitions#system" target="_blank" rel="noopener noreferrer">systems</a>, NFPA 16. <br>
                    4.	Dry-<a href="https://up.codes/viewer/virginia/va-fire-code-2012/chapter/2/definitions#chemical" target="_blank" rel="noopener noreferrer">chemical</a> extinguishing <a href="https://up.codes/viewer/virginia/va-fire-code-2012/chapter/2/definitions#system" target="_blank" rel="noopener noreferrer">systems</a>, NFPA 17.<br>
                    5.	Wet-<a href="https://up.codes/viewer/virginia/va-fire-code-2012/chapter/2/definitions#chemical" target="_blank" rel="noopener noreferrer">chemical</a> extinguishing <a href="https://up.codes/viewer/virginia/va-fire-code-2012/chapter/2/definitions#system" target="_blank" rel="noopener noreferrer">systems</a>, NFPA 17A. <br> <br>

                    <b>Exception:</b> Factory-built commercial cooking recirculating <a href="https://up.codes/viewer/virginia/va-fire-code-2012/chapter/2/definitions#system" target="_blank" rel="noopener noreferrer">systems</a> that are tested in accordance with UL 710B and listed, <a href="https://up.codes/viewer/virginia/va-fire-code-2012/chapter/2/definitions#labeled" target="_blank" rel="noopener noreferrer">labeled</a> and installed in accordance with Section 304.1 of the <i>International Mechanical Code</i>.
                </p>
                <h4 class="title text-left text-black">904.11.2 System Interconnection</h4>
                <p class="text-left">
                    The actuation of the fire extinguishing <a href="https://up.codes/viewer/virginia/va-fire-code-2012/chapter/2/definitions#system" target="_blank" rel="noopener noreferrer">system</a> shall automatically shut down the fuel or electrical power supply to the cooking equipment. The fuel and electrical supply reset shall be manual.
                </p>
                <h4 class="title text-left text-black">904.11.4.1 Listed Sprinklers</h4>
                <p class="text-left">
                    Sprinklers used for the protection of fryers shall be tested in accordance with UL 199E, listed for that application and installed in accordance with their listing.
                </p>
                <h4 class="title text-left text-black">904.11.5 Portable Fire Extinguishers for Commercial Cooking Equipment</h4>
                <p class="text-left">
                    Portable fire extinguishers shall be provided within a 30-foot (9144 mm) travel distance of commercial-type cooking equipment. Cooking equipment involving <a href="https://up.codes/viewer/virginia/va-fire-code-2012/chapter/2/definitions#solid" target="_blank" rel="noopener noreferrer">solid</a> fuels or vegetable or animal oils and fats shall be protected by a Class K rated portable extinguisher in accordance with <a href="https://up.codes/viewer/virginia/va-fire-code-2012/chapter/9/fire-protection-systems#904.11.5.1" target="_blank" rel="noopener noreferrer">Section 904.11.5.1</a> or <a href="https://up.codes/viewer/virginia/va-fire-code-2012/chapter/9/fire-protection-systems#904.11.5.2" target="_blank" rel="noopener noreferrer">904.11.5.2</a>, as applicable.
                </p>
                <h4 class="title text-left text-black">904.11.5.1 Portable Fire Extinguishers for Solid Fuel Cooking Appliances</h4>
                <p class="text-left">
                    All <a href="https://up.codes/viewer/virginia/va-fire-code-2012/chapter/2/definitions#solid" target="_blank" rel="noopener noreferrer">solid</a> fuel cooking appliances, whether or not under a <a href="https://up.codes/viewer/virginia/va-fire-code-2012/chapter/2/definitions#hood" target="_blank" rel="noopener noreferrer">hood</a>, with fireboxes 5 cubic feet (0.14 m3) or less in volume shall have a minimum 2.5-gallon (9 L) or two 1.5-gallon (6 L) Class K wet-<a href="https://up.codes/viewer/virginia/va-fire-code-2012/chapter/2/definitions#chemical" target="_blank" rel="noopener noreferrer">chemical</a> portable fire extinguishers located in accordance with <a href="https://up.codes/viewer/virginia/va-fire-code-2012/chapter/9/fire-protection-systems#904.11.5" target="_blank" rel="noopener noreferrer">Section 904.11.5</a>.
                </p>
                <h4 class="title text-left text-black">904.11.5.2 Class K Portable Fire Extinguishers for Deep Fat Fryers</h4>
                <p class="text-left">
                    When hazard areas include deep fat fryers, listed Class K portable fire extinguishers shall be provided as follows: <br><br>

                    1.	For up to four fryers having a maximum cooking medium capacity of 80 pounds (36.3 kg) each: one Class K portable fire extinguisher of a minimum 1.5-gallon ( 6 L) capacity. <br>
                    2.	For every additional group of four fryers having a maximum cooking medium capacity of 80 pounds (36.3 kg) each: one additional Class K portable fire extinguisher of a minimum 1.5-gallon (6 L) capacity shall be provided. <br>
                    3.	For individual fryers exceeding 6 square feet (0.55 m2) in surface area: Class K portable fire extinguishers shall be installed in accordance with the extinguisher manufacturer's recommendations.
                </p>
                <h4 class="title text-left text-black">904.11.6 Operations and Maintenance</h4>
                <p class="text-left">
                    <a href="https://up.codes/viewer/virginia/va-fire-code-2012/chapter/2/definitions#automatic_fire_extinguishing_system" target="_blank" rel="noopener noreferrer">Automatic fire-extinguishing systems</a> protecting commercial cooking <a href="https://up.codes/viewer/virginia/va-fire-code-2012/chapter/2/definitions#system" target="_blank" rel="noopener noreferrer">systems</a> shall be maintained in accordance with Sections <a href="https://up.codes/viewer/virginia/va-fire-code-2012/chapter/9/fire-protection-systems#904.11.6.1" target="_blank" rel="noopener noreferrer">904.11.6.1</a> through <a href="https://up.codes/viewer/virginia/va-fire-code-2012/chapter/9/fire-protection-systems#904.11.6.3" target="_blank" rel="noopener noreferrer">904.11.6.3</a>.
                </p>
                <h4 class="title text-left text-black">904.11.6.1 Existing Automatic Fire-Extinguishing Systems</h4>
                <p class="text-left">
                    Where changes in the cooking media, positioning of cooking equipment or replacement of cooking equipment occur in <a href="https://up.codes/viewer/virginia/va-fire-code-2012/chapter/2/definitions#existing" target="_blank" rel="noopener noreferrer">existing</a> commercial cooking <a href="https://up.codes/viewer/virginia/va-fire-code-2012/chapter/2/definitions#system" target="_blank" rel="noopener noreferrer">systems</a>, the <a href="https://up.codes/viewer/virginia/va-fire-code-2012/chapter/2/definitions#automatic_fire_extinguishing_system" target="_blank" rel="noopener noreferrer">automatic fire-extinguishing system</a> shall be required to comply with the applicable provisions of Sections <a href="https://up.codes/viewer/virginia/va-fire-code-2012/chapter/9/fire-protection-systems#904.11" target="_blank" rel="noopener noreferrer"></a> through <a href="https://up.codes/viewer/virginia/va-fire-code-2012/chapter/9/fire-protection-systems#904.11.4" target="_blank" rel="noopener noreferrer">904.11.4</a>.
                </p>
                <h4 class="title text-left text-black">904.11.6.2 Extinguishing System Service</h4>
                <p class="text-left">
                    <a href="https://up.codes/viewer/virginia/va-fire-code-2012/chapter/2/definitions#automatic_fire_extinguishing_system" target="_blank" rel="noopener noreferrer">Automatic fire-extinguishing systems</a> shall be serviced at least every six months and after activation of the <a href="https://up.codes/viewer/virginia/va-fire-code-2012/chapter/2/definitions#system" target="_blank" rel="noopener noreferrer">system</a>. Inspection shall be by qualified individuals, and a certificate of inspection shall be forwarded to the <a href="https://up.codes/viewer/virginia/va-fire-code-2012/chapter/2/definitions#fire_code_official" target="_blank" rel="noopener noreferrer">fire code official</a> upon completion.
                </p>
                <h4 class="title text-left text-black">904.11.6.3 Fusible Link and Sprinkler Head Replacement</h4>
                <p class="text-left">
                    Fusible links and <a href="https://up.codes/viewer/virginia/va-fire-code-2012/chapter/2/definitions#automatic" target="_blank" rel="noopener noreferrer">automatic</a> sprinkler heads shall be replaced at least annually, and other protection devices shall be serviced or replaced in accordance with the manufacturer's instructions. <br><br>

                    <b>Exception:</b> Frangible bulbs are not required to be replaced annually.
                </p>
                <div class="text-center"><a href="https://up.codes/viewer/virginia/ifc-2015" target="_blank" class="btn-get-started scrollto">Read more about The International Fire Code Commercial Kitchen Hood Systems</a></div>
            </header>
        </div>
    </section><!-- #about -->

    @include('homepage.partials.download')
</main>

@endsection
