<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Fire Safe Kitchen Inspections - @yield('title')</title>

    <link rel="icon" href="{{ URL::asset('/img/logo.png') }}" type="image/x-icon" />

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="{{ asset('css/styles.css') }}" rel="stylesheet">

</head>

<body>

    @yield('content')

    <script src="{{ asset('js/scripts.js') }}"></script>
    
    @yield('scripts')
</body>

</html>