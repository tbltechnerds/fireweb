<!DOCTYPE html>
<html prefix="og: http://ogp.me/ns#" lang="en">
<head>
    <meta charset="utf-8">
    <title>@yield('title') | Fire Inspection Pro</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta content="width=device-width, initial-scale=1.0" name="viewport">
	<meta name="author" content="Denver Web Development">
	<meta content="Fire Inspection Pro" name="description">
    <meta content="Fire Inspection Pro" name="keywords">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="base-url" content="{{ url('/') }}">

    <!-- OG Image -->
    <meta property="og:title" content="Fire Inspection Pro"/>
	<meta property="og:type" content="website"/>
	<meta property="og:url" content="/"/>
	<meta property="og:site_name" content="Fire Inspection Pro"/>
	<meta property="og:description" content="Fire Inspection Pro"/>

    <meta property="og:image" content="{{ asset('frontpage/img/og-image.png') }}" rel="stylesheet" />
	<meta property="og:image:secure_url" content="{{ asset('frontpage/img/og-image.png') }}" rel="stylesheet" />
	<meta property="og:image:alt" content="Fire Inspection Pro" />
	<meta property="og:image:type" content="image/jpeg" />
	<meta property="og:image:width" content="1200" />
    <meta property="og:image:height" content="630" />

    <!-- Favicons -->
    <link href="{{ asset('frontpage/img/favicon.ico') }}" rel="stylesheet" rel="shortcut icon">
    <link href="{{ asset('frontpage/img/favicon.ico') }}" rel="stylesheet" rel="apple-touch-icon">
    <link href="{{ asset('frontpage/img/favicon-16x16.png') }}" rel="stylesheet" rel="icon" type="image/png" sizes="16x16" />

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Montserrat:300,400,500,700" rel="stylesheet">

    <!-- Bootstrap CSS File -->
    <link href="{{ asset('frontpage/lib/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">

    <!-- Libraries CSS Files -->
    <link href="{{ asset('frontpage/lib/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ asset('frontpage/lib/animate/animate.min.css') }}" rel="stylesheet">
    <link href="{{ asset('frontpage/lib/ionicons/css/ionicons.min.css') }}" rel="stylesheet">
    <link href="{{ asset('frontpage/lib/owlcarousel/assets/owl.carousel.min.css') }}" rel="stylesheet">
    <link href="{{ asset('frontpage/lib/lightbox/css/lightbox.min.css') }}" rel="stylesheet">
    <link href="{{ asset('frontpage/lib/swipe/photoswipe.css') }}" rel="stylesheet">
    <link href="{{ asset('frontpage/lib/swipe/default-skin/default-skin.css') }}" rel="stylesheet">
    <link href="{{ asset('frontpage/lib/sweetalert/sweetalert.min.css') }}" rel="stylesheet">
    <link href="{{ asset('frontpage/lib/loader/loading.min.css') }}" rel="stylesheet">

    <!-- Main Stylesheet File -->
    <link href="{{ asset('frontpage/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('frontpage/css/custom.css') }}" rel="stylesheet">
    <link href="{{ asset('frontpage/css/responsive.css') }}" rel="stylesheet">
    @yield('styles')
</head>
<body>

    @yield('content')

    {{-- @if (!request()->is('firesafety'))
        @include('homepage.partials.footer')
    @endif --}}
    <!-- jQuery  -->
    <!-- JavaScript Libraries -->
    <script src="{{ asset('frontpage/lib/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('frontpage/lib/jquery/jquery-migrate.min.js') }}"></script>
    <script src="{{ asset('frontpage/lib/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('frontpage/lib/easing/easing.min.js') }}"></script>
    <script src="{{ asset('frontpage/lib/superfish/hoverIntent.js') }}"></script>
    <script src="{{ asset('frontpage/lib/superfish/superfish.min.js') }}"></script>
    <script src="{{ asset('frontpage/lib/wow/wow.min.js') }}"></script>
    <script src="{{ asset('frontpage/lib/owlcarousel/owl.carousel.min.js') }}"></script>
    <script src="{{ asset('frontpage/lib/lightbox/js/lightbox.min.js') }}"></script>
    <script src="{{ asset('frontpage/lib/touchSwipe/jquery.touchSwipe.min.js') }}"></script>
    <script src="{{ asset('frontpage/lib/swipe/photoswipe.min.js') }}"></script>
    <script src="{{ asset('frontpage/lib/swipe/photoswipe-ui-default.min.js') }}"></script>
    <script src="{{ asset('frontpage/lib/sweetalert/sweetalert.min.js') }}"></script>
    <script src="{{ asset('frontpage/lib/loader/loading.min.js') }}"></script>
    <!-- Template Main Javascript File -->
    <script src="{{ asset('frontpage/js/main.js') }}"></script>
    <script src="{{ asset('frontpage/js/init_swipe.js') }}"></script>
    @stack('scripts')
</body>
</html>
