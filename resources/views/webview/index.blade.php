@extends('layouts.frontend')

{{-- @section('content') --}}
<div class="container-fluid px-0">
    <div id="imgCarousel" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner bg-primary" role="listbox">
            @foreach ($photos as $photo)
            <div class="carousel-item @if ($loop->first) active @endif">
                <div class="d-flex align-items-center justify-content-center">
                    <img class="d-block img-fluid vh-100" src="{{ asset('/storage/uploads/photos/' . $photo->photo) }}"
                        alt="{{ $photo->title }}">
                    <div class="carousel-caption">
                        <h1 class="lead text-uppercase text-dark">{{ $photo->title }}</h1>
                        <p class="text-dark">{{ $photo->description }}</p>
                    </div>
                </div>
            </div>
            @endforeach
        </div>

        <a class="carousel-control-prev" href="#imgCarousel" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>

        <a class="carousel-control-next" href="#imgCarousel" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
</div>
{{-- @endsection --}}