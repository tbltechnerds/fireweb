<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'AppController@index')->name('index');
Route::get('/about')->uses('HomeController@about')->name('about');
Route::get('/firesafety')->uses('HomeController@firesafety')->name('firesafety');
Route::get('/view-images/{id}')->uses('HomeController@viewImages')->name('view-images');

Auth::routes();

Route::prefix('/')->middleware(['auth', 'two_factor_auth'])->namespace('Dashboard')->group( function(){

    Route::resource('dashboard/photos', 'Photos\PhotosController');
    Route::resource('dashboard/category', 'Category\CategoryController');
    Route::resource('dashboard/profile-settings', 'UserSettings\UserSettingsController');

    //Change password
    Route::get('dashboard/change_password', 'UserSettings\UserSettingsController@change_password_form')->name('change_password');
    Route::post('dashboard/change_password', 'UserSettings\UserSettingsController@change_password')->name('change_password');

    //Restore Category
    Route::get('dashboard/category/restore/{category}', 'Category\CategoryController@restore')->name('restore');
    Route::post('dashboard/category/restore/{category}', 'Category\CategoryController@restore')->name('restore');

    //Restore Photos
    Route::get('dashboard/photos/restore/{photo}', 'Photos\PhotosController@restore');
    Route::post('dashboard/photos/restore/{photo}', 'Photos\PhotosController@restore');

    Route::get('/dashboard', 'HomeController@index')->name('dashboard');
});

Route::get('/2fa', 'TwoFactorController@show2faForm');
Route::post('/2fa', 'TwoFactorController@verifyToken');

