<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Admin',
            'profile_pic' => '',
            'email' => 'ducreg07@gmail.com',
            'password' => Hash::make('password'),
        ]);
    }
}
