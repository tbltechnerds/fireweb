/**********     >>>>>>>>>> PLEASE USE ES6 FUNCTION FORMAT THANKS <<<<<<<<<<<<<    **********/
// Base URL
var base_url = $('meta[name="base-url"]').attr('content');
// CSRF Token
var csrf_token    = $('meta[name="csrf-token"]').attr('content');

//View Image
var viewImage = (id)=>{
    H5_loading.show({color:"rgb(255, 255, 255)",background:"rgba(0, 0, 0, 0.55)",timeout:0.5,scale:0.8});
    $.ajax({
        headers: {'X-CSRF-TOKEN': csrf_token},
        url: base_url+'/view-images/'+id,
        dataType:'JSON',
        success: (res)=>{
            H5_loading.hide();
            if(res.code==200){
                // Item Title
                $('#item-title').html('<a href="javascript:;">'+res.data.title+'</a>');

                // Item Description
                var desc = res.data.description ? res.data.description :  'No description.';
                $('#item-desc').html(desc);

                // Item Images
                var photos = '';
                if(res.data.photo){
                    var imgs = res.data.photo.split('|');
                    $.each(imgs, function(index, imgName) {
                        photos += '<div class="single-image"><img src="'+base_url+'/storage/uploads/photos/'+imgName+'" alt="Gallery Images"></div>';
                        console.log(imgName)
                    });
                    $('#item-images').html(photos);

                    // Re-initialize Carousel Display)
                    var owl = $('#item-images');
                    owl.trigger('destroy.owl.carousel');
                    owl.owlCarousel({
                        autoplay: false,
                        dots: false,
                        loop: false,
                        center: true,
                        nav : true,
                        navText : ["<i class='fa fa-chevron-left'></i>","<i class='fa fa-chevron-right'></i>"],
                        items: 1
                    });
                }else{
                    // Default image display if no photos
                    photos = '<div class="single-image"><img src="'+base_url+'/storage/uploads/default_image.png" alt="Gallery Images"></div>';
                    $('#item-images').html(photos);
                }
                $('#viewImageModal').modal('show');
            }else{
                swal('Oops!', 'An error occured. Please refresh your page', 'error');
            }
        }
    });
}
