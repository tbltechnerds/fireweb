<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;

class TwoFactorController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function show2faForm()
    {
        return view('2fa.2fa');
    }
    
    public function verifyToken(Request $request)
    {
        $this->validate($request, [
            'token' => 'required|string',
        ]);

        $user = auth()->user();

        if ($request->token === $user->two_factor_token) {
            $user->two_factor_expiry = Carbon::now()->addMinutes(config('session.lifetime'));
            
            $user->save(); 

            return redirect()->intended('dashboard');
        }

        return redirect('/2fa')->withErrors(['token' => 'Incorrect code.']);
    }
}
