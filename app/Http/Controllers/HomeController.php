<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Dashboard\Photo;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['about', 'firesafety', 'viewImages']]);
    }

    public function index()
    {
        return view('dashboard.index');
    }

    public function about()
    {
        return view('homepage.about');
    }

    public function firesafety()
    {
        $photos = Photo::all();
        return view('homepage.gallery', compact('photos'));
    }

    public function viewImages($id)
    {
        $images = Photo::find($id);
        return ($images) ? $this->response(200, 'Data fetched successfully.', $images) : $this->response(204, 'No Content.', []);
    }
}
