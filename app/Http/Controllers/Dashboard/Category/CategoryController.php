<?php

namespace App\Http\Controllers\Dashboard\Category;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Dashboard\Category;
use DataTables;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $data = Category::all();
        // Category::where('id', 3)->withTrashed()->get();
        if(request()->ajax())
        {
            return DataTables::of(Category::withTrashed()->latest()->get())
                ->addColumn('action', function($data){
                    if($data->deleted_at == null) {
                    $actionButtons = '<a href="/dashboard/category/'.$data->id.'/edit" class="mr-2 btn btn-warning editBtn">
                                        <i class="fas fa-pencil-alt"></i>
                                        </a>
                                        <form action="" style="display: inline">
                                            '.csrf_field().'
                                            <button type="button" class="mr-2 btn btn-danger deleteBtn" onclick="deleteData('.$data->id.')">
                                                <i class="fas fa-trash-alt"></i>
                                            </button>
                                        </form>
                                        <button type="button" class="btn btn-primary" disabled style="opacity: .5;">
                                            <i class="fas fa-trash-restore"></i>
                                        </button>';
                    return $actionButtons;
                    } else{
                        $actionButtons = '<a class="mr-2 btn btn-warning editBtn" style="pointer-events: none; cursor: default; opacity: .5;">
                                            <i class="fas fa-pencil-alt"></i>
                                        </a>
                                        <button class="mr-2 btn btn-danger deleteBtn" style="pointer-events: none; cursor: default; opacity: .5;">
                                            <i class="fas fa-trash-alt"></i>
                                        </button>
                                        <form action="/dashboard/category/restore/'.$data->id.'" method="POST" style="display: inline;>
                                        '.csrf_field().'
                                            <button type="button" class="btn btn-primary" onclick="restoreData('.$data->id.')">
                                                <i class="fas fa-trash-restore"></i>
                                            </button>
                                        </form>';

                        return $actionButtons;
                    }
                })
                ->rawColumns(['action'])
                ->make(true);
               
        };

        return view('dashboard.category.index', compact('data'));
    }

    public function getId($id)
    {
        $data = Category::find($id);

        return $data;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.category.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'category' => 'required',
        ]);

        $categories = new Category;

        $categories->category_name = $request->input('category');

        $categories->save();

        return redirect()->route('category.index')->with('success', 'Added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = Category::find($id);

        return view('dashboard.category.edit', compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $category = Category::find($id);

        $category->category_name = $request->input('category');

        $category->save();

        return redirect()->route('category.index')->with('success', 'Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = Category::find($id);

        $category->delete();

        return redirect()->route('category.index')->with('delete-success', 'Deleted');
    }

    public function restore($id)
    {
        Category::withTrashed()->find($id)->restore();
        
    }
}
