<?php

namespace App\Http\Controllers\Dashboard\UserSettings;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\User;
use Auth;

class UserSettingsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = User::all();

        return view('dashboard.profile_settings.index', compact('user'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = User::find($id);

        $data->name = $request->input('name');
        $data->email = $request->input('email');
        $data->two_factor_option = $request->input('twoFactorAuth') == "on" ? 1 : 0;

        if ($request->has('profile_pic')) {

            $fileNameWithExt = $request->file('profile_pic')->getClientOriginalName();
            
            // get file name
            $filename = pathinfo($fileNameWithExt, PATHINFO_FILENAME);

            // get extension
            $extension = $request->file('profile_pic')->getClientOriginalExtension();

            $fileNameToStore = $filename.'_'.time().'.'.$extension;

            // upload
            $path = $request->file('profile_pic')->storeAs('public/profile_pic', $fileNameToStore);

            $data->profile_pic = $fileNameToStore;

        }

        $data->save();

        return redirect()->route('profile-settings.index')->with('success', 'Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function change_password_form() {

        return view('dashboard.profile_settings.change_password');

    }

    public function change_password(Request $request) {

        if(!(Hash::check($request->get('current_password'), Auth::user()->password))) {

            return back()->with('error', 'Your current password does not match');

        }

        if(strcmp($request->get('current_password'), $request->get('new_password')) == 0) {

            return back()->with('error', 'Your current password cannot be the same with the new password');

        }

        $request->validate([
            'current_password' => 'required',
            'password' => 'required|string|min:8|confirmed',
        ]);

        $user = Auth::user();
        $user->password = Hash::make($request->get('password'));
        $user->save();
            
        return back()->with('success', 'Password change successfully');

    }
}
