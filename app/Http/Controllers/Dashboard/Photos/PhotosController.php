<?php

namespace App\Http\Controllers\Dashboard\Photos;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Dashboard\Photo;
use App\Models\Dashboard\Category;
use DataTables;
use File;

class PhotosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(request()->ajax())
        {
            return DataTables::of(Photo::withTrashed()->latest()->get())
                ->editColumn('photo', function($data){

                $images = explode('|', $data->photo);
                $imgUrl = $data->photo ? 'photos/'.$images[0] : 'default_image.png';
                $photo = '
                    <a href="/storage/uploads/'.$imgUrl.'" class="image-popup" title="'.$data->title.'">
                        <div class="tbl-img">
                            <img src="/storage/uploads/'.$imgUrl.'" alt="'.$data->title.'">
                        </div>
                    </a>
                ';
                    return $photo;
                })

                ->editColumn('category_name', function($data){

                    $category = $data->category->category_name;

                    return $category;
                })

                ->editColumn('title',function($data){
                    return \Str::limit(strip_tags($data->title),10);

                 })

                ->editColumn('description',function($data){
                    return \Str::limit(strip_tags($data->description),30);

                 })

                ->addColumn('action', function($data){
                    if($data->deleted_at == null) {
                    $actionButtons = '<a href="/dashboard/photos/'.$data->id.'/edit" class="mr-2 btn btn-warning editBtn">
                                        <i class="fas fa-pencil-alt"></i>
                                        </a>
                                        <form action="" style="display: inline;">
                                            '.csrf_field().'
                                            <button type="button" class="btn btn-danger deleteBtn" onclick="deleteData('.$data->id.')">
                                                <i class="fas fa-trash-alt"></i>
                                            </button>
                                        </form>
                                        <button type="button" class="btn btn-primary" disabled style="opacity: .5;">
                                            <i class="fas fa-trash-restore"></i>
                                        </button>';
                    return $actionButtons;
                    } else{
                        $actionButtons = '<a class="mr-2 btn btn-warning editBtn" style="pointer-events: none; cursor: default; opacity: .5;">
                                            <i class="fas fa-pencil-alt"></i>
                                        </a>
                                        <button class="mr-2 btn btn-danger deleteBtn" style="pointer-events: none; cursor: default; opacity: .5;">
                                            <i class="fas fa-trash-alt"></i>
                                        </button>
                                        <form action="/dashboard/photos/restore/'.$data->id.'" method="POST" style="display: inline;>
                                        '.csrf_field().'
                                            <button type="button" class="btn btn-primary" onclick="restoreData('.$data->id.')">
                                                <i class="fas fa-trash-restore"></i>
                                            </button>
                                        </form>';
                    return $actionButtons;
                    }
                })
                ->rawColumns(['action', 'photo', 'category_name', 'title', 'description'])
                ->make(true);
        };

        return view('dashboard.photos.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();
        return view('dashboard.photos.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'description' => 'required',
            'category' => 'required',
            // 'photo' => 'required|image|nullable|mimes:jpeg,png,jpg,gif,svg',

        ]);

        $input=$request->all();
            $images=array();
            $files = $request->input('website_image');
            if($files){
                foreach($files as $file) {
                    $filename = 'img_'.uniqid().'.png';
                    $dataImg  = explode(',', $file);
                    $decoded  = base64_decode($dataImg[1]);
                    file_put_contents(storage_path(). '/app/public/uploads/photos/'.$filename,$decoded);
                    $images[]=$filename;
                }
            }

        $data = new Photo;
        $data->title = $request->input('title');
        $data->description = $request->input('description');
        $data->category_id = $request->input('category');
        $data->photo = implode("|",$images);

        $data->save();

        return redirect()->route('photos.index')->with('success', 'Added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $categories = Category::all();
        $data = Photo::find($id);
        return view('dashboard.photos.edit', compact('data', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $data = Photo::find($id);
        $data->title = $request->input('title');
        $data->description = $request->input('description');
        $data->category_id = $request->category_id;

        $input=$request->all();
            $images=array();
            $files = $request->input('website_image');
            if($files){
                foreach($files as $file){
                    $name    = 'img_'.uniqid().'.png';
                    $dataImg = explode(',', $file);
                    $decoded = base64_decode($dataImg[1]);
                    file_put_contents(storage_path(). '/app/public/uploads/photos/'.$name,$decoded);
                    $images[]=$name;
                }
            }

            if($files=$request->input('old_image')){
                foreach($files as $file){
                    $images[]=$file;
                }
            }

        $data->photo = implode('|', $images);

        $data->save();

        return redirect()->route('photos.index')->with('success', 'Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Photo::find($id);

        $deleteImage = Photo::where('id', $id)->first();

        $filePath = storage_path('app/public/uploads/photos');
        $fileToDelete = $filePath . '/' . $deleteImage->photo;

        File::delete($fileToDelete);

        $data->delete();

        return redirect()->route('photos.index')->with('delete-success', 'Deleted');
    }

    public function restore($id)
    {
        Photo::withTrashed()->find($id)->restore();
        
    }
}
