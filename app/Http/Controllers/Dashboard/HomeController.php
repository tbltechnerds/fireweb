<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;;
use App\Models\Dashboard\Category;
use App\Models\Dashboard\Photo;

class HomeController extends Controller
{
    public function index(){

        $photos = Photo::count();
        $category = Category::count();
        
        return view('dashboard.index', compact(['photos', 'category']));
    }
    
}
